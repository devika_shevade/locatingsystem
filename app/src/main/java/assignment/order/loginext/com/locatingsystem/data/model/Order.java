package assignment.order.loginext.com.locatingsystem.data.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.IntDef;

import java.io.Serializable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;

/**
 * Created by Devika on 12/09/16.
 */
public class Order implements Serializable{//ToDo : Devika: Implement parcealable

    @IntDef({STATUS_QUEUED, STATUS_IN_TRANSIT, STATUS_DELIVERED,STATUS_CANCELLED})
    @Retention(RetentionPolicy.SOURCE)
    public @interface StatusType {}

    public static final int STATUS_QUEUED = 0;
    public static final int STATUS_IN_TRANSIT = 1;
    public static final int STATUS_DELIVERED = 2;
    public static final int STATUS_CANCELLED = 3;

    @StatusType
    public int getStatusType(){
            return this.status;
    }
    @StatusType
    public  void setStatusType(@StatusType int status){
        this.status = status;
    }

    protected String id;
    protected @Order.StatusType int status;
    private Customer customer;
    protected ArrayList<PackageItem> itemList;


    public ArrayList<PackageItem> getItemList() {
        return itemList;
    }

    public void setItemList(ArrayList<PackageItem> itemList) {
        this.itemList = itemList;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
