package assignment.order.loginext.com.locatingsystem;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.lang.ref.WeakReference;

/**
 * Created by Devika on 12/09/16.
 */
public class OrderApplication extends Application{
    private static WeakReference<OrderApplication> wApp = new WeakReference<>(null);
    @Override
    public void onCreate() {
        super.onCreate();
        wApp.clear();
        wApp = new WeakReference<>(this);
    }

    public static Context getContext() {
        OrderApplication app = wApp.get();

        return app != null ? app.getApplicationContext() : null;
    }


}
