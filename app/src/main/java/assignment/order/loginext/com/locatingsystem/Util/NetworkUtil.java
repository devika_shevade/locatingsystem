package assignment.order.loginext.com.locatingsystem.Util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import assignment.order.loginext.com.locatingsystem.OrderApplication;

/**
 * Created by Devika on 12/09/16.
 */
public class NetworkUtil {
    public static boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) OrderApplication.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm != null) {
            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            return activeNetwork != null &&
                    activeNetwork.isConnectedOrConnecting();
        }
        return false;
    }
}
