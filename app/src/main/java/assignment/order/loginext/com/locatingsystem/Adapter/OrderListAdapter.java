package assignment.order.loginext.com.locatingsystem.Adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import assignment.order.loginext.com.locatingsystem.R;
import assignment.order.loginext.com.locatingsystem.data.model.Customer;
import assignment.order.loginext.com.locatingsystem.data.model.Order;
import assignment.order.loginext.com.locatingsystem.data.model.PackageItem;
import assignment.order.loginext.com.locatingsystem.fragment.order.OrderListFragment;

/**
 * Created by Devika on 13/09/16.
 */


public class OrderListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static final int ORDER = 0;
    public static final int PACKAGE_LIST = 1;

    public static final int EXPAND_STATE_TRUE = 1;
    public static final int EXPAND_STATE_FALSE = 0;
    public static final int EXPAND_STATE_NOT_CALC = -1;

    LayoutInflater mInflater;
    String mQueued, mInTransit, mDelivered, mCancelled;
    Activity mBase;
    ArrayList mData;


    public OrderListAdapter(Activity base, ArrayList<Order> data) {
        this.mData = data;
        mInflater = (LayoutInflater) base.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mQueued = base.getResources().getString(R.string.order_status_queued);
        mInTransit = base.getResources().getString(R.string.order_status_transit);
        mDelivered = base.getResources().getString(R.string.order_status_delivered);
        mCancelled = base.getResources().getString(R.string.order_status_cancelled);
        this.mBase = base;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int type) {
        switch (type) {
            case ORDER:
                return createOrderViewHolder();
            case PACKAGE_LIST:
               return createItemNameHolder(parent.getContext());
        }
        return null;
    }

    private OrderViewHolder createOrderViewHolder(){
       View view = mInflater.inflate(R.layout.order_view, null, false);
        final OrderViewHolder orderViewHolder = new OrderViewHolder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Order order = (Order) mData.get(orderViewHolder.getAdapterPosition());
                if(OrderListAdapter.this.mBase instanceof OrderListFragment.OnOrderSelectedListener) {
                    ((OrderListFragment.OnOrderSelectedListener) mBase).OnOrderSelected(order);
                }
            }
        });
        orderViewHolder.parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Order order = (Order) mData.get(orderViewHolder.getAdapterPosition());
                if(orderViewHolder.expandedState == EXPAND_STATE_TRUE){
                    int range = 0;
                    int pos = mData.indexOf(order);
                    while (mData.size() > pos + 1 && mData.get(pos + 1) instanceof PackageItem) {
                        mData.remove(pos + 1);
                        range++;
                    }
                    notifyItemRangeRemoved(pos + 1, range);
                    orderViewHolder.expansionView.setImageResource(R.drawable.ic_arrow_down);
                    orderViewHolder.expandedState = EXPAND_STATE_FALSE;
                }else{
                    int startPos = mData.indexOf(order);
                    int index = startPos + 1;
//                    int ItemListsize = order.getItemList();
                    for (PackageItem i : order.getItemList()) {
                        mData.add(index, i);
                        index++;
                    }
                    notifyItemRangeInserted(startPos + 1, index - startPos - 1);
                    orderViewHolder.expansionView.setImageResource(R.drawable.ic_arrow_up);
                    orderViewHolder.expandedState = EXPAND_STATE_TRUE;
                }
            }
        });

        return orderViewHolder;
    }

    private ItemNameHolder createItemNameHolder(Context context){
        TextView itemTextView = new TextView(context);
        itemTextView.setLayoutParams(
                new ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT));
        return new ItemNameHolder(itemTextView);
    }

    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        switch (getItemViewType(position)) {
            case ORDER:
                bindOrderView((Order) mData.get(position),(OrderViewHolder)holder,position);
                break;
            case PACKAGE_LIST:
                bindPackageItem((PackageItem) mData.get(position), (ItemNameHolder) holder);
                break;
        }
    }

    private void bindPackageItem(final PackageItem item,final ItemNameHolder holder ){
        holder.itemText.setText(item.getPackageTitle());
    }

    private  void bindOrderView(final Order order, final OrderViewHolder orderViewHolder,int position){
        Customer customer = order.getCustomer();
        orderViewHolder.name.setText(customer.getName());
        orderViewHolder.addr.setText(customer.getAddr());
        orderViewHolder.phno.setText(customer.getPhno());
        ArrayList<PackageItem> items = order.getItemList();

        int itemCount = items.size();
        if(itemCount == 1) orderViewHolder.itemCount.setText(itemCount + " Item");
        else orderViewHolder.itemCount.setText(itemCount + " Items");

        orderViewHolder.expandedState = getExpansionState(position);
        if(orderViewHolder.expandedState == EXPAND_STATE_TRUE)  orderViewHolder.expansionView.setImageResource(R.drawable.ic_arrow_up);
        else orderViewHolder.expansionView.setImageResource(R.drawable.ic_arrow_down);
        orderViewHolder.status.setText(getStatuString(order.getStatusType()));
    }

    @Override
    public int getItemViewType(int position) {
        Object object = mData.get(position);
        if(object instanceof Order){
            return ORDER;
        }else{
            return PACKAGE_LIST;
        }
    }

    @Override
    public int getItemCount() {
        if(mData != null) return mData.size();
        return 0;
    }

    /*package*/static class OrderViewHolder extends RecyclerView.ViewHolder {
        protected TextView name,addr,phno,itemCount,status;
        protected ImageView expansionView;
        protected View parent;
        int expandedState = -1;


        public OrderViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.order_view_name);
            addr = (TextView) itemView.findViewById(R.id.order_view_addr);
            phno = (TextView) itemView.findViewById(R.id.order_view_phno);
            status = (TextView) itemView.findViewById(R.id.order_view_status);
            itemCount = (TextView) itemView.findViewById(R.id.order_view_item_count);
            expansionView = (ImageView) itemView.findViewById(R.id.order_view_expansion);
            parent = itemView.findViewById(R.id.order_view_parent);
        }
    }

    static class ItemNameHolder extends RecyclerView.ViewHolder{
        TextView itemText;
        public ItemNameHolder(View itemView) {
            super(itemView);
            itemText = (TextView)itemView;
        }
    }

    public String getStatuString(@Order.StatusType int status){
        switch (status){
            case Order.STATUS_QUEUED : return mQueued;
            case Order.STATUS_IN_TRANSIT : return mInTransit;
            case Order.STATUS_DELIVERED : return mDelivered;
            case Order.STATUS_CANCELLED : return mCancelled;
            default: return "";
        }
    }

    private int getExpansionState(int pos){
        if(pos+1 < mData.size() && mData.get(pos) instanceof Order && mData.get(pos + 1) instanceof PackageItem){
            return EXPAND_STATE_TRUE;
        }else{
            return EXPAND_STATE_FALSE;
        }
    }

}
