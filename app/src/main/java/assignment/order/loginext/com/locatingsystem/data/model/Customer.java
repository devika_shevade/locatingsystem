package assignment.order.loginext.com.locatingsystem.data.model;

import java.io.Serializable;

/**
 * Created by Devika on 12/09/16.
 */
public class Customer implements Serializable{
    protected String name;
    protected String addr;
    protected String phno;
    protected String customerId;


    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }
    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhno() {
        return phno;
    }

    public void setPhno(String phno) {
        this.phno = phno;
    }


}
