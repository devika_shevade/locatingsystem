package assignment.order.loginext.com.locatingsystem.activity.order;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;

import java.util.ArrayList;

import assignment.order.loginext.com.locatingsystem.activity.BaseActivity;
import assignment.order.loginext.com.locatingsystem.R;
import assignment.order.loginext.com.locatingsystem.activity.tracklocation.LocationTrackingActivity;
import assignment.order.loginext.com.locatingsystem.data.OrderManager;
import assignment.order.loginext.com.locatingsystem.data.model.Order;
import assignment.order.loginext.com.locatingsystem.fragment.order.OrderDetailFragment;
import assignment.order.loginext.com.locatingsystem.fragment.order.OrderListFragment;

public class OrderListActivity extends BaseActivity implements OrderListFragment.OnOrderSelectedListener,OrderDetailFragment.OnOrderUpdatedListener{
//    private SwipeRefreshLayout mRefreshLayout;
//    private Recy
    LinearLayout fragmentContainer;
    OrderListFragment mOrderListFragment;
    private OrderManager mOrderManager;
    private OrderDetailFragment mOrderDetailFragment;
    private ArrayList mOrderList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(savedInstanceState == null) {
            mOrderManager = OrderManager.getInstance();
            mOrderList = new ArrayList<Order>(mOrderManager.fetchOrders());
           addOrderListWithData(mOrderList);
        }


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(OrderListActivity.this, LocationTrackingActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_order_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void OnOrderSelected(Order order) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        mOrderDetailFragment = OrderDetailFragment.newInstance(order);
        mOrderList = mOrderListFragment.getCurrentList();
        transaction.remove(mOrderListFragment);
        transaction.replace(R.id.order_fragment_layout, mOrderDetailFragment, "OrderDEtail");
        transaction.commit();
        mOrderListFragment = null;
    }

    @Override
    public void refreshData() {
        mOrderList.clear();
        mOrderList.addAll(mOrderManager.fetchOrders());
        mOrderListFragment.notifyAdapter();
    }


//    @Override
//    public void notifyOrderUpdate(Order order) {
//        mOrderListFragment.notifyAdapter();
//    }

    @Override
    public void OnOrderUpdated(Order order) {

    }
    private void addOrderListWithData(ArrayList dataList){
        mOrderListFragment = OrderListFragment.newInstance(dataList);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.add(R.id.order_fragment_layout, mOrderListFragment, "orderList");
        if(mOrderDetailFragment != null) {
            transaction.remove(mOrderDetailFragment);
            mOrderDetailFragment = null;
        }
        transaction.commit();
    }

    @Override
    public void onBackPressed() {
        if(mOrderListFragment == null) addOrderListWithData(mOrderList);
        else super.onBackPressed();
     }
}
