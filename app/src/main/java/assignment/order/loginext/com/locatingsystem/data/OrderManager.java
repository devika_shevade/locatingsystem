package assignment.order.loginext.com.locatingsystem.data;

import java.util.ArrayList;

import assignment.order.loginext.com.locatingsystem.Util.NetworkUtil;
import assignment.order.loginext.com.locatingsystem.data.model.Customer;
import assignment.order.loginext.com.locatingsystem.data.model.Order;
import assignment.order.loginext.com.locatingsystem.data.model.PackageItem;

/**
 * Created by Devika on 12/09/16.
 */
public class OrderManager {
    private static final int OFFSET = 10;
    private static final String ORDER_ID_QUALIFIER = "ord_";
    private static final String CUSTOMER_ID_QUALIFIER = "cst_";
    private static final String PACKAGE_ITEM_QUALIFIER = "pkg_";
    private static OrderManager orderManagerInstance;
    private ArrayList<Order>orderData;

    private OrderManager(){
        orderData = new ArrayList<>();
    }

    public static synchronized OrderManager getInstance(){
        if(orderManagerInstance == null) orderManagerInstance = new OrderManager();
        return orderManagerInstance;
    }
    /*Devika:
    method to fetch oredrs either from db or over network
    * */
    public ArrayList<Order> fetchOrders(){
//        if(NetworkUtil.isOnline()){
            for(int i=0; i<OFFSET; i++){
                Order order = createOrder(i);
                if(order != null) orderData.add(i,order);

            }
//        }
        return orderData;
    }

    private static Order createOrder(int val){
        Order order = new Order();
        order.setStatusType(Order.STATUS_QUEUED);
        order.setId(ORDER_ID_QUALIFIER + val);
        Customer customer = new Customer();
        customer.setAddr("dummy soc" + val + ", dummy block, dummy city ");
        customer.setName(" Dummy Customer" + val);
        customer.setPhno("09876543210");
        customer.setCustomerId(CUSTOMER_ID_QUALIFIER + val);
        order.setCustomer(customer);
        ArrayList<PackageItem> packageItems = new ArrayList<>();
        switch (val%3){
            case 0: packageItems.add(createPackageItem("1_"+val));
                break;
            case 1:
                packageItems.add(createPackageItem("1_"+val));
                packageItems.add(createPackageItem("2_"+val));
                packageItems.add(createPackageItem("3_"+val));
                break;
            case 2: packageItems.add(createPackageItem("1_"+val));
                packageItems.add(createPackageItem("2_"+val));
                packageItems.add(createPackageItem("3_"+val));
                packageItems.add(createPackageItem("4_"+val));
                packageItems.add(createPackageItem("5_"+val));
                break;
        }
        order.setItemList(packageItems);
        if(packageItems != null) return order;//Devika : consideration for improper order over n/w
        else return null;
    }

    private static PackageItem createPackageItem(String val){
        PackageItem packageItem = new PackageItem();
        String id = PACKAGE_ITEM_QUALIFIER+val;
        packageItem.setId(id);
        packageItem.setPackageDesc(id +" dummy desc");
        packageItem.setPackageTitle(id+" Title");
        return packageItem;
    }
    public boolean updateOrder(Order order){
        if(NetworkUtil.isOnline()) {
            //DEvika : sychronize with server
        }else{
            //DEvika : store locally
        }
        return true;
    }
}
