package assignment.order.loginext.com.locatingsystem.fragment.order;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import assignment.order.loginext.com.locatingsystem.Adapter.OrderListAdapter;
import assignment.order.loginext.com.locatingsystem.R;
import assignment.order.loginext.com.locatingsystem.data.model.Order;
import assignment.order.loginext.com.locatingsystem.fragment.BaseFragment;

public class OrderListFragment extends BaseFragment {

    private RecyclerView mRecyclerView;
    private OrderListAdapter mAdapter;
    private OnOrderSelectedListener mListener;
    private Bundle savedState = null;
    ArrayList<Order> mOrderList;
    private SwipeRefreshLayout mRefreshLayout;

    public OrderListFragment() {
    }

    public static OrderListFragment newInstance(ArrayList<Order> orderList) {
        OrderListFragment fragment = new OrderListFragment();
        fragment.mOrderList = orderList;
        return fragment;
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnOrderSelectedListener) {
            mListener = (OnOrderSelectedListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnOrderUpdatedListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view =  inflater.inflate(R.layout.fragment_order_list, container, false);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.order_recylerview);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mAdapter = new OrderListAdapter(getActivity(),mOrderList);
        mRecyclerView.setAdapter(mAdapter);

        mRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.order_swiperefresh);
        mRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        //DEvika : update data from server
                        mListener.refreshData();
                    }
                }
        );
        return view;
    }
    public ArrayList getCurrentList(){
        return mOrderList;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void clearMemory() {
        mOrderList = null;
    }

    public void notifyAdapter() {
        if(mAdapter != null) mAdapter.notifyDataSetChanged();
        mRefreshLayout.setRefreshing(false);
    }

    public interface OnOrderSelectedListener {
         void OnOrderSelected(Order order);
        void refreshData();
    }
}
