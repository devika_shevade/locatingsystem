package assignment.order.loginext.com.locatingsystem.data.model;

import java.io.Serializable;

/**
 * Created by Devika on 12/09/16.
 */
public class PackageItem implements Serializable{
    protected String id;
    protected String packageTitle;
    protected String packageDesc;


    public String getPackageDesc() {
        return packageDesc;
    }

    public void setPackageDesc(String packageDesc) {
        this.packageDesc = packageDesc;
    }

    public String getPackageTitle() {
        return packageTitle;
    }

    public void setPackageTitle(String packageTitle) {
        this.packageTitle = packageTitle;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
