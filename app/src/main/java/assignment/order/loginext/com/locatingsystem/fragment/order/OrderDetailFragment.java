package assignment.order.loginext.com.locatingsystem.fragment.order;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import assignment.order.loginext.com.locatingsystem.R;
import assignment.order.loginext.com.locatingsystem.data.model.Customer;
import assignment.order.loginext.com.locatingsystem.data.model.Order;
import assignment.order.loginext.com.locatingsystem.fragment.BaseFragment;

/**
 *
 *
 * Activities that contain this fragment must implement the
 * {@link OnOrderUpdatedListener} interface
 * to handle interaction events.
 * Use the {@link OrderDetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class OrderDetailFragment extends BaseFragment {
    private Order mOrder;
    private Button mUpdateState;

    private OnOrderUpdatedListener mListener;

    public OrderDetailFragment() {
        // Required empty public constructor
    }

    public static OrderDetailFragment newInstance(Order order) {
        OrderDetailFragment fragment = new OrderDetailFragment();
        fragment.mOrder = order;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_order_detail, container, false);
        mUpdateState = (Button) view.findViewById(R.id.order_detail_update);
        setButtonText();

        TextView orderNo = (TextView) view.findViewById(R.id.order_detail_view_order_no);
        orderNo.setText(mOrder.getId());

        Customer customer = mOrder.getCustomer();
        TextView name  = (TextView) view.findViewById(R.id.order_detail_view_name);
        name.setText(customer.getName());

        TextView addr = (TextView) view.findViewById(R.id.order_detail_view_addr);
        addr.setText(customer.getAddr());

        TextView phno = (TextView) view.findViewById(R.id.order_detail_view_phno);
        phno.setText(customer.getPhno());

        Button cancelButton = (Button) view.findViewById(R.id.order_detail_cancel);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOrder.setStatusType(Order.STATUS_CANCELLED);
                mListener.OnOrderUpdated(mOrder);
                mUpdateState.setVisibility(View.GONE);
            }
        });
        mUpdateState.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateOrder();
                mListener.OnOrderUpdated(mOrder);
            }
        });
        return view;
    }

    private void setButtonText(){
        switch (mOrder.getStatusType()){
            case Order.STATUS_QUEUED : mUpdateState.setText("Move to IN-TRANSIT");
                break;
            case Order.STATUS_DELIVERED :
                mUpdateState.setText("DELIVERED");
                mUpdateState.setClickable(false);
                break;
            case Order.STATUS_IN_TRANSIT : mUpdateState.setText("Move to DELIVERED");
            break;
            case Order.STATUS_CANCELLED : mUpdateState.setText("CANCELLED");
                mUpdateState.setClickable(false);
                break;
        }
    }

    private void updateOrder(){
        switch (mOrder.getStatusType()){
            case Order.STATUS_QUEUED:
                mOrder.setStatusType(Order.STATUS_IN_TRANSIT);
                break;

            case Order.STATUS_IN_TRANSIT : mOrder.setStatusType(Order.STATUS_DELIVERED);
                break;
            case Order.STATUS_DELIVERED :break;
            case Order.STATUS_CANCELLED :mOrder.setStatusType(Order.STATUS_CANCELLED);
                break;
        }
        setButtonText();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnOrderUpdatedListener) {
            mListener = (OnOrderUpdatedListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnOrderUpdatedListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void clearMemory() {

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnOrderUpdatedListener {
        void OnOrderUpdated(Order order);
    }

}
