package assignment.order.loginext.com.locatingsystem.fragment;

//import android.app.Fragment;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;

/**
 * Created by Devika on 12/09/16.
 */
public abstract class BaseFragment extends Fragment {

    @Override
    public void onDestroyView() {
        clearMemory();
        unbindView(getView());
        super.onDestroyView();
    }

    public abstract void clearMemory();

    public void unbindView(View v) {
        int count = ((ViewGroup) v).getChildCount();
        for (int i = 0; i < count; i++) {
            try {
                View view = ((ViewGroup) v).getChildAt(i);
                if (view instanceof ViewGroup && !(view instanceof AdapterView) && !(view instanceof ViewPager) && !(view instanceof RecyclerView))
                    unbindView(view);
                else
                    ((ViewGroup) v).removeViewAt(i);
                clearView(view);
            } catch (Exception e) {
                System.out.println("View Exception found" + e.getMessage());
            }
        }
    }

    private void clearView(View view) {

        if (view instanceof RecyclerView) {
            ((RecyclerView) view).setAdapter(null);
        }

        if (view instanceof Toolbar) {
            ((Toolbar) view).setNavigationOnClickListener(null);
            ((Toolbar) view).setOnMenuItemClickListener(null);
            ((Toolbar) view).removeAllViews();
        }
        view.setOnLongClickListener(null);
        view.setOnTouchListener(null);

    }

}
